# class-options-gui.coffee --
# Copyright (C) 2019 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Class option GUI module
# 
# Provides options to edit classes and their attributes and methods.
#

# Small interface to edit the method data.
#
# This is the GUI placed on the right toolbar. It is used to edit the method
# information.
class MethodGui
    constructor: (@parent) ->
        @method_name = ''
        this.load_form()

    load_form: () ->
        $.get './uml/html/class-method.html', (result) =>
            @main_div = $(result)
            $(@parent).append @main_div

            @txt_name = @main_div.find '.method-name'

            this.update_widgets()
            this.assign_events()

    update_widgets: () ->
        @txt_name.val @method_name

    assign_events: () ->
        # btn = @main_div.find 'button.btn-delete'
        # btn.on 'click', () ->

    update_data: () ->
        @method_name = @txt_name.val()
            
    # Return the method string representation as it has to appear on the
    # UML Class Cell.
    #
    # @return {string} A prefix:suffix or simply a suffix.
    get_attr: () ->
        this.update_data()
        @method_name

    show: () ->
        @main_div.show()

    hide: () ->
        @main_div.hide()


# Manage the Attributes widgets.
class AttrGui
    constructor: (@parent) ->
        @attr =
            prefix: ''
            name: 'attr'
        this.load_form()

    load_form: () ->
        $.get './uml/html/class-attr.html', (result) =>
            @main_div = $(result)
            $(@parent).append @main_div

            @txt_name = @main_div.find 'input.attributeClass'
            @txt_prefix = @main_div.find 'input.attr-prefix'

            this.update_widgets()
            this.assign_events()

    # Set the attribute data.
    #
    # @param attr {object} The attribute to set.
    # @options prefix {string}
    # @options name {string}
    set_attr: (attr) ->
        @attr = attr
        this.update_widgets()

    update_widgets: () ->
        @txt_name.val @attr.name
        @txt_prefix.val @attr.prefix

    update_data: () ->
        @attr.name = @txt_name.val()
        @attr.prefix = @txt_prefix.val()

    assign_events: () ->
        # btn = @main_div.find 'button.btnDeleteInputClass'
        # btn.on 'click', () ->

    # Return the data used to show on the UML class.
    #
    # This is used to append this string to the UML Class Cell
    #
    # @return {string} A prefix:suffix abbreviated PREFIX.
    get_attr: () ->
        this.update_data()
        @attr.prefix + ":" + @attr.name
            
    show: () ->
        @main_div.show()
    hide: () ->
        @main_div.hide()


# Show the options for a UML Class
#
# A small GUI placed at the right-side toolbar. It is used to edit a class
# data, its attributes and its methods.
class ClassOptionsGui
    constructor: (@parent="#menuOpciones") ->
        @cell = null
        @lst_attrs = []
        @lst_methods = []

        @main_div = null
        @attr_div = null
        @method_div = null
        @txt_prefix = null
        @txt_name = null
        
        this.load_form()

    load_form: () ->
        $.get './uml/html/class-options.html', (result) =>
            @main_div = $(result)

            @attr_div = @main_div.find '.attr-list'
            @method_div = @main_div.find '.method-list'
            @txt_prefix = @main_div.find '.txt-prefix'
            @txt_name = @main_div.find '.txt-name'
            
            $(@parent).prepend @main_div
            this.assign_events()
            this.hide()
            console.log 'class-options form loaded'

    assign_events: () ->
        btn = @main_div.find '.attributes .btn-add'
        btn.on 'click', () =>
            this.add_attr()
        btn = @main_div.find '#btnAddMethodClass'
        btn.on 'click', () =>
            this.add_method()

        btn = @main_div.find '.btn-done'
        btn.on 'click', () =>
            this.confirm()
        btn = @main_div.find '.btn-cancel'
        btn.on 'click', () =>
            this.hide()

    add_method: () ->
        method = new MethodGui @method_div
        @lst_methods.push method

    add_attr: () ->
        attr = new AttrGui @attr_div
        @lst_attrs.push attr

    confirm: () ->
        this.update_data()
        this.hide()

    # Set the class JointJS cell to manage.
    #
    # @param class_cell {object} The joint.dia.Cell object.
    set_cell: (class_cell) ->
        @cell = class_cell
        this.update_widgets()

    set_cellview: (cellview) ->
        @cell = cellview.model
        this.update_widgets()

    # Generate the name to show on the UML Class Cell.
    # 
    # @return {string} The prefix:suffix
    get_name: () ->
        @txt_prefix.val() + ":" + @txt_name.val()

    # Update each widget information with the cell data.
    update_widgets: () ->
        unless @cell?
            return
        @txt_prefix.val @cell.prop 'data/prefix'
        @txt_name.val @cell.prop 'name'
        # TODO: Update attributes
        # TODO: Update methods

    # Update the cell associated with this interface.
    update_data: () ->
        lst_attrs = @lst_attrs.map (attr) ->
            attr.get_attr()
        lst_methods = @lst_methods.map (method) ->
            method.get_attr()

        @cell.prop
            name: this.get_name()
            'data/prefix': @txt_prefix.val()
            attributes: lst_attrs
            methods: lst_methods

    show: () ->
        @main_div.show()

    hide: () ->
        @main_div.hide()

add_method = (cls, value="") ->
    childs = $("#listMethods").children().length + 1

    divmethod = $('<div>').attr
        id: 'mts_' + actualClass.id + '_' + childs
        class: 'input-group input-group-sm mb-1'

    divgrp = $('<div>').attr
        class: 'input-group-append'
    
    methodinput=$('<input>').attr
        class: 'methodClass form-control'
        type: 'text'
    methodinput.val value

    delbtn = $('<button>').attr
        class: 'btnDeleteInputClass btn btn-sm btn-danger'
        type: 'button'
        value: '-'
        onclick: 'deleteInput(mts_' + actualClass.id + '_' + childs +')'
    delbtn.html '-'

    divmethod.append methodinput
    divgrp.append delbtn
    divmethod.append divgrp
    
    $("#listMethods").append divmethod

# What to do when the user cancels the class options
cancel_menu_class = () ->
    $("#listAttributes").empty()
    actualClass = null
    $("#menuClass").hide()

confirm_menu_class = () ->
    changeNameClass()
    changeAttributes()
    changeMethods()
    resizeClass actualClass.model
    # vuelve a su tamaño por lo que ya no esta compacta.
    actualClass.model.attr 'customAttr/isCompact', false
    actualClass = null
    $("#menuClass").hide()


export class_options_gui = new ClassOptionsGui
    
export show = () ->
    $('#menuClass').show()

export hide = () ->
    $('#menuClass').hide()

export initialize = () ->

