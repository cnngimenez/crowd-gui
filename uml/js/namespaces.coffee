# Namespace
#
# @namespace namespace
class Namespace

    constructor: (prefix, uri) ->
        @prefix = prefix
        @uri = uri


class NamespaceServer
    url = 'http://crowd.fi.uncoma.edu.ar/crowd2-namespaces/namespaces/prefixs/'
    # url = '../../crowd2-namespaces/namespaces/'
    

    constructor: () ->
        @lst = []

    # Request all the namespaces list from the user
    #
    # @param user_id [Number] The user id.
    by_user: (user_id, callback) ->
        $.getJSON url + 'by_user/' + user_id, (results) =>
            results.forEach (ns) =>
                @lst.push new Namespace(ns.prefix, ns.iri)
                callback @lst


export namespace_server = new NamespaceServer
