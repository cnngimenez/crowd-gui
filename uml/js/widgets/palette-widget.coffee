# palette-widget.coffee --
# Copyright (C) 2020 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { styles } from '../style.js'
import { FlyPaper } from './flypaper.js'


# Palette Widget
#
# This is the palette toolbar at the left-side of the crowd editor.
#
# The widget order its elements as a grid. See order_elts(). After creating
# this widget, set what to do after the user drops an element to the main_paper.
# 
# @namespace widgets
export class PaletteWidget

    # Create an instance of a palette.
    #
    # @param selector {string} The div tag selector.
    # @param main_paper {joint.dia.Paper} The paper where to drop the elements.
    # @param on_mouse_up_hook {function} What to do after the user drop an
    #   element to the main_paper. See FlyPaper.
    constructor: (selector, main_paper, @on_mouse_up_hook) ->
        @parent = document.querySelector selector
        @graph = new joint.dia.Graph()
        @paper = new joint.dia.Paper
            el: @parent
            width: this.get_width()
            height: this.get_height()
            model: @graph
            interactive: false
        @elements = []
        @next_col = 0
        @next_row = 0
        this.create_uml_elements()

        @flypaper = new FlyPaper main_paper, @on_mouse_up_hook

        this._assign_events()

    update_widget: () ->
        this.order_elts()

    # Re-order the elements on the widget according to the grid layout.
    #
    # Use a grid layout of 3 x 1.
    order_elts: () ->
        cellheight = this.get_height() / 3
        cellwidth = this.get_width() 

        # x, y is the top-left corner
        for elt, i in @elements
            do (elt, i) =>
                # center: at half of the grid-cell, move half of the figure width.
                x = cellwidth / 2 - elt.get('size').width / 2
                y = i * cellheight
                y += cellwidth / 2 - elt.get('size').height / 2
                elt.set 'position',
                    x: x
                    y: y

    get_width: () ->
        widthcss = window.getComputedStyle(@parent).width
        return parseFloat widthcss.replace('px', '')

    get_height: () ->
        heightcss = window.getComputedStyle(@parent).height
        return parseFloat heightcss.replace('px', '')

    # Create elements for editing UML class diagram.
    create_uml_elements: () ->
        cellwidth = this.get_width() / 2
        elt = new joint.shapes.uml.Class
            name: 'Class'
            size: styles.classes.size
            attrs: styles.classes.attrs
        @elements.push elt

        elt = new joint.shapes.erd.Relationship
            name: 'Relationship'
            attrs: styles.relationships.attrs
            size: styles.relationships.size
        @elements.push elt

        elt = new joint.shapes.erd.Inheritance
            name: 'Is-A'
            attrs: styles.is_a.attrs
            size: styles.is_a.size
        @elements.push elt

        @graph.addCell elt for elt in @elements

        this.update_widget()

    # Is the given coordinates inside the widget?
    # 
    # @param x, y {number}
    # @return {boolean}
    is_in_widget: (x, y) ->
        origin = @paper.$el.offset()
        x > origin.left and x < origin.left + @paper.$el.width() and
        y > origin.top and y < origin.top + @paper.$el.height()

    # What to do when the user clicks on an element of the
    # palette (i.e. starts a drag-and-drop event).
    #
    # @param cellView {joint.dia.CellView}
    # @param e {Event}
    # @param x,y {number}
    on_cell_pointer_down: (cellView, e, x, y) ->
        # First show it, if not, the scale() on set_shape()
        # won't work.
        @flypaper.show()
        @flypaper.set_shape cellView

    on_flypaper_drop: (evt) ->
        @flypaper.on_mouse_up()
        
    _assign_events: () ->
        @paper.on 'cell:pointerdown', (cellview, e, x, y) =>
            this.on_cell_pointer_down cellview, e, x, y

    # Get a new element by cloning one that is using on the flypaper.
    #
    # Basically, get a clone on the element that is on the flypaper. This is
    # used to insert it on the main paper.
    #
    # @return {joint.dia.Cell} The cell model cloned from the one on the
    #   flypaper.
    get_new_element: () ->
        @flypaper.get_new_element()
