# diagram-widget.coffee --
# Copyright (C) 2020 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# DiagramWidget
#
export class DiagramWidget

    constructor: (div_selector) ->
        @parent = document.querySelector div_selector
        @graph = new joint.dia.Graph()
        @paper = new joint.dia.Paper
            el: @parent
            width: this.get_width()
            height: this.get_height()
            model: @graph
            gridSize: 10
            snapLinks:
                radius: 75
            linkPinning: false
            markAvailable: true
            interactive:
                vertexAdd: true

        this.assign_events()

    get_width: () ->
        widthcss = window.getComputedStyle(@parent).width
        return parseFloat(widthcss.replace('px', ''))

    get_height: () ->
        heightcss = window.getComputedStyle(@parent).height
        return parseFloat(heightcss.replace('px', ''))

    # Assign events to the elements managed by this instance.
    #
    # Usually, this method is called at instance initialization. Except when
    # the paper and graph is generated externally.
    assign_events: () ->
        @paper.on 'element:pointerclick', this.on_element_click
        @paper.on 'cell:pointerdblclick', this.on_dbl_click
        @paper.on 'element.pointermove', this.on_pointer_move
        @paper.on 'mousewheel DOMMouseScroll', this.on_mouse_wheel

    # Is the given coordinates inside the paper?
    #
    # @param x, y {number}
    # @return {boolean}
    is_in_paper: (x, y) ->
        target = @paper.$el.offset()
        width = @paper.$el.width()
        height = @paper.$el.height()
        x > target.left and x < target.left + width and \
        y > target.top and y < target.top + height

    # What to do when the user does click on an element in the paper.
    #
    # This is a handler for the element:pointerclick event.
    #
    # @param cell_view {joint.dia.CellView} the cell view clicked.
    # @param evt {Event}
    on_element_click: (cell_view, evt) ->
        @current_element = cell_view
        switch window.getType cell_view
            when 'Class' then this.show_class_tools cell_view
            when 'Inheritance' then this.show_gen_tools cell_view
            when 'Nary' then this.show_nary_tools cell_view

    # What to do when the user drops and is using the flypaper.
    #
    # @param evt {Event}
    # @param new_element {joint.dia.Cell}
    on_flypaper_drop: (evt, new_element) ->
        x = evt.pageX
        y = evt.pageY
        if this.is_in_paper x, y
            relative_point = @paper.clientToLocalPoint evt.clientX, evt.clientY
            elt_x = relative_point.x - (new_element.attributes.size.width / 2)
            elt_y = relative_point.y - (new_element.attributes.size.height / 2)
            new_element.position elt_x, elt_y

            @graph.addCell new_element
