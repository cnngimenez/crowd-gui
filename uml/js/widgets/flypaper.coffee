# flypaper.coffee --
# Copyright (C) 2020 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



# FlyPaper
#
# A paper that can fly under the mouse.
#
# This is used when the user drags an element from the palette and drops it on
# the main diagram.
#
# @namespace widgets
export class FlyPaper

    # @param main_paper {joint.dia.Paper}
    # @param on_mouse_up_hook {function} A function called when the mouse is
    #   released. The synopsis is `function (evt)` where evt is a JS Event.
    constructor: (@main_paper, @on_mouse_up_hook) ->
        @div = $('<div id="flyPaper"></div>')
        $('body').append @div
        @graph = new joint.dia.Graph;
        @paper = new joint.dia.Paper
            el: $('#flyPaper')
            model: @graph
            interactive: false

        @cellmodel = null
        @pos = null
        @offset =
            x: 0
            y: 0

        @div.hide()

        this._assign_events()

    # @param cell {joint.dia.Cell}
    set_shape: (cell) ->
        @cellmodel = cell.model.clone()
        @pos = @cellmodel.position()
        @cellmodel.position 0, 0
        @graph.addCell @cellmodel
        
        @offset =
            x: @cellmodel.attributes.size.width / 2 * @main_paper.scale().sx
            y: @cellmodel.attributes.size.height / 2 * @main_paper.scale().sy

        @paper.scale @main_paper.scale().sx, @main_paper.scale().sy

        @cellview = @paper.findViewByModel @cellmodel
        @cellview.update()

    # Update the position where the mouse is.
    #
    # @param evt {Event}
    on_mouse_move: (evt) ->
        @div.offset
            left: evt.pageX - @offset.x
            top: evt.pageY - @offset.y

    # When releasing the mouse click (i.e. doing the dropping) remove the
    # cellmodel and remove the events.
    #
    # @param evt {Event}
    on_mouse_up: (evt) ->
        $('body').off 'mousemove.fly'
        $('body').off 'mouseup.fly'

        # @cellmodel.remove()
        @div.remove()  # Delete the flypaper instead of hiding it

    _assign_events: () ->
        $('body').on 'mousemove.fly', (evt) =>
            this.on_mouse_move evt
        $('body').on 'mouseup.fly', (evt) =>
            @on_mouse_up_hook evt
            this.on_mouse_up evt

    # Get a clone from the model used on this flypaper.
    #
    # @return {joint.dia.Cell} A JointJS model.
    get_new_element: () ->
        @cellmodel.clone()

    show: () ->
        @div.show()
    hide: () ->
        @div.hide()

