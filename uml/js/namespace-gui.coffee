# namespace-gui.coffee --
# Copyright (C) 2020 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Purpose:
# Load dialogs and assign events needed to make the GUI usable to view
# namespaces prefix-IRI mapping.
# 

import {namespace_server} from './namespaces.js'

class NamespaceGui
    
    constructor: (@parent_id="#plugins") ->
        this.load_form()

    # Load the HTML component.
    load_form: () ->
        $(@parent_id).html ""
        $.get './uml/html/namespace-gui.html', (result) =>
            $(@parent_id).append result
            this.assign_events()
            this.on_load false
            console.log "namespace-gui form loaded"

    # Assign events to the namespace form.
    assign_events: () ->
        $("button#namespace-load").on 'click', () =>
            this.load_namespaces()

        console.log "Events assigned"

    load_namespaces: (user_id=null) ->
        this.on_load true
        if not user_id?
            user_id = $("#namespace-userid").val()
        namespace_server.by_user user_id, () =>
            this.update_namespaces()
            this.on_load false

    update_namespaces: () ->
        table = $("#namespace-list")
        table.html ""
        namespace_server.lst.forEach (ns) =>
            tr = $("<tr>")
            tdprefix = $("<td>").html ns.prefix
            tdiri = $("<td>").html ns.uri

            tr.append tdprefix
            tr.append tdiri
            table.append tr
            
    on_load: (enable) ->
        spinner = $("button#namespace-load").find ".spinner-grow"
        tblspinner = $("#namespace-table").find ".spinner-border"
        table = $("#namespace-table").find ".table"
        if enable
            tblspinner.show()
            table.hide()
            spinner.show()
        else
            tblspinner.hide()
            table.show()
            spinner.hide()

    show: () ->
        $("#namespace-gui").modal 'show'

    hide: () ->
        $("#namespace-gui").modal 'hide'
    
export namespace_gui = new NamespaceGui

# Assign events to the main GUI
assign_events = () ->
    $("#btn_namespaces").on 'click', () ->
        namespace_gui.show()

$.when($.ready).then () ->
    assign_events()
