// Generated by CoffeeScript 2.5.1
var MainGui;

import {
  class_options_gui
} from './class-options-gui.js';

import {
  namespace_gui
} from './namespace-gui.js';

import {
  PaletteWidget
} from './widgets/palette-widget.js';

import {
  DiagramWidget
} from './widgets/diagram-widget.js';

// Manage the main window, the navbar and main components.

// Creates subcomponents.
MainGui = class MainGui {
  constructor() {
    this.diagram_widget = new DiagramWidget('#paper');
    this.namespace_gui = namespace_gui;
    this.palette_widget = new PaletteWidget("#palette", this.diagram_widget.paper, (e) => {
      return this._flypaper_mouse_up(e);
    });
    this.class_options_gui = class_options_gui;
  }

  assign_events() {
    // TODO: Move this events to NavbarGui
    $("#btn_namespaces").on('click', function() {
      return namespace_gui.show();
    });
    return $("body").on('mouseup.fly', (evt) => {
      return this._flypaper_mouse_up(evt);
    });
  }

  // What to do when the use drops while the flypaper is in use.

  // This is here, because the knowledge needed to create the shape when
  // dropping. It needs the main_paper, the flypaper, the type of cell, etc.
  // In other words, this method is responsible to create the cell and to call
  // the mouse_up on the flypaper (i.e. to pass responsability to hide the
  // flypaper).

  // @param e {Event}
  _flypaper_mouse_up(evt) {
    var new_element, x, y;
    x = evt.pageX;
    y = evt.pageY;
    if (!this.palette_widget.is_in_widget(evt)) {
      new_element = this.palette_widget.get_new_element();
      this.diagram_widget.on_flypaper_drop(evt, new_element);
    }
    return this.palette_widget.on_flypaper_drop(evt);
  }

};

export var main_gui = new MainGui();

window.main_gui = main_gui; // For debugging purposes

$.when($.ready).then(function() {
  return main_gui.assign_events();
});
