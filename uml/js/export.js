// Generated by CoffeeScript 2.5.1
// export.coffee --
// Copyright (C) 2019 GILIA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Convert a Class Cell into a JSON representation

// @elt {Cell} A Cell model from JointJS
var assoc_to_json, awc_to_json, class_to_json, classify_elt, get_class_id_from_link, isa_to_json;

class_to_json = function(elt) {
  return {
    name: elt.attr('.uml-class-name-text/text'),
    id: elt.cid,
    iri: elt.attr('data-class-url'),
    attributes: elt.attributes.attributes,
    attribute_iris: elt.attr('data-attr-iris'),
    methods: elt.attributes.methods,
    position: elt.position(),
    size: elt.size()
  };
};

// Return the id to the class associated to this link.

// Each link has two elements: a Class and the inheritance or the association
// name rectangle.

// @param link {Link} a JointJS Link
get_class_id_from_link = function(link) {
  var source;
  source = link.getSourceCell();
  if (source.attributes.type !== 'uml.Class') {
    return link.getTargetCell().cid;
  } else {
    return source.cid;
  }
};

// Convert the Inheritance (IS-A) cell into a JSON

// This function also gathers the cells representing the associated classes
// for retrieving their ids.

// @param elt {Cell} A JointJS cell representing the circle figure.
isa_to_json = function(elt) {
  var child_links, links, parent_links, subclass_ids, superclass_ids;
  links = graphMain.getConnectedLinks(elt);
  // Split links into links to parents and to children.
  parent_links = links.filter(function(elt) {
    return elt.attr('customAttr').inheritance;
  });
  child_links = links.filter(function(elt) {
    return !elt.attr('customAttr').inheritance;
  });
  superclass_ids = parent_links.map(get_class_id_from_link);
  subclass_ids = child_links.map(get_class_id_from_link);
  return {
    id: elt.cid,
    type: elt.attr('text/text'),
    superClasses: superclass_ids,
    subClasses: subclass_ids,
    position: elt.position(),
    size: elt.size()
  };
};

// Convert the Association With Class cell into a JSON

// This function also gathers the cells representing the associated classes
// for retrieving their ids.

// @param elt {Cell} A JointJS cell representing the diamond figure.
awc_to_json = function(elt) {
  var class_ids, links;
  links = graphMain.getConnectedLinks(elt);
  class_ids = links.map(get_class_id_from_link);
  return {
    id: elt.cid,
    classAssociation: class_ids,
    position: elt.position(),
    size: elt.size()
  };
};

assoc_to_json = function(elt) {
  var class_ids, links;
  links = graphMain.getConnectedLinks(elt);
  class_ids = links.map(get_class_id_from_link);
  return {
    id: elt.cid,
    info: {
      cardOrigin: elt.attributes.labels[0].attrs.text.text,
      cardDestino: elt.attributes.labels[1].attrs.text.text,
      roleOrigin: elt.attributes.labels[2].attrs.text.text,
      roleDestiny: elt.attributes.labels[3].attrs.text.text,
      nameAssociation: elt.attributes.labels[4].attrs.text.text,
      roleOriginIRI: elt.attr('data-role-origin-iri'),
      roleTargetIRI: elt.attr('data-role-target-iri'),
      assocIRI: elt.attr('data-assoc-iri')
    },
    source: elt.getTargetCell().cid,
    target: elt.getSourceCell().cid,
    type: 'binaryAssociation'
  };
};

// Add a JSON according to the type of cell into output.

// @param elt {Cell} A JointJS cell
// @param output {object} A Json object where the generated json is inserted.
classify_elt = function(elt, output) {
  switch (elt.attributes.type) {
    case "uml.Class":
      return output.classes.push(class_to_json(elt));
    case "erd.Inheritance":
      return output.inheritances.push(isa_to_json(elt));
    case "erd.Relationship":
      return output.assocWithClass.push(awc_to_json(elt));
    case "standard.Link":
      if (elt.attr('customAttr/type') === 'binaryAssociation') {
        return output.associations.push(assoc_to_json(elt));
      }
  }
};

// Generate a JSON representation from the UML diagram.

// @return {object} A JSON representation that can be used for exporting the
//   UML diagram.
export var generate_json = function() {
  var elt, i, len, lst_elts, output;
  lst_elts = graphMain.getElements();
  lst_elts = lst_elts.concat(graphMain.getLinks());
  output = {
    classes: [],
    inheritances: [],
    associations: [],
    assocWithClass: []
  };
  for (i = 0, len = lst_elts.length; i < len; i++) {
    elt = lst_elts[i];
    classify_elt(elt, output);
  }
  return output;
};
