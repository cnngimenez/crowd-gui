// Generated by CoffeeScript 2.5.1
// options-general.coffee --
// Copyright (C) 2019 GILIA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
var options;

options = options != null ? options : {};

// Provides the General Options interface

// @mixin options.general
options.general = {
  create_html: function() {
    var btn, div, hidediv;
    div = $("<div>").attr('class', 'menu-gral m-2');
    div.append($("<hr/>"));
    hidediv = $("<div>").attr('class', 'btn-group btn-group-sm');
    btn = $("<button>");
    btn.append($('<i>').attr({
      class: 'fas fa-search-minus'
    }));
    btn.append(' Hide methods');
    btn.attr('class', 'btn btn-secondary btn-hide-methods');
    hidediv.append(btn);
    btn = $("<button>");
    btn.append($('<i>').attr({
      class: 'fas fa-search-plus'
    }));
    btn.append(' Show methods');
    btn.attr('class', 'btn btn-secondary btn-show-methods');
    hidediv.append(btn);
    div.append(hidediv);
    btn = $("<button>");
    btn.append($('<i>').attr({
      class: 'fas fa-broom'
    }));
    btn.append(' Clear diagram');
    btn.attr('class', 'btn btn-danger btn-clear-all btn-sm');
    div.append(btn);
    return div;
  },
  add_html: function() {
    return $('#menuOpciones').append(options.general.create_html());
  },
  show: function() {
    return $('.menu-gral').show();
  },
  hide: function() {
    return $('.menu-gral').hide();
  },
  // Hide all methods and attributes from all the classes.
  hide_all_methods: function() {
    var all_cells, all_classes, cell, i, len, results;
    all_cells = graphMain.getCells();
    // These are elements from the palette
    all_cells = all_cells.concat(paletteGraph.getCells());
    
    // Filter only the classes
    all_classes = all_cells.filter(function(elt) {
      return elt.attributes.type === 'uml.Class';
    });
// Hide methods and attributes for each class
    results = [];
    for (i = 0, len = all_classes.length; i < len; i++) {
      cell = all_classes[i];
      results.push((function(cell) {
        var view;
        cell.attr('.uml-class-methods-text/text', '');
        cell.attr('.uml-class-attrs-text/text', '');
        cell.attr('.uml-class-methods-rect').height = 0;
        cell.attr('.uml-class-attrs-rect').height = 0;
        cell.attr('.uml-class-name-rect').height = 40;
        cell.attr('customAttr/isCompact', true);
        view = paper.findViewByModel(cell);
        if (view == null) {
          view = palette.findViewByModel(cell);
        }
        return view.update();
      })(cell));
    }
    return results;
  },
  show_all_methods: function() {
    var all_cells, all_classes, cell, i, len, results;
    all_cells = graphMain.getCells();
    // These are elements from the palette
    all_cells = all_cells.concat(paletteGraph.getCells());
    // Filter only the classes
    all_classes = all_cells.filter(function(elt) {
      return elt.attributes.type === 'uml.Class';
    });
// Hide methods and attributes for each class
    results = [];
    for (i = 0, len = all_classes.length; i < len; i++) {
      cell = all_classes[i];
      results.push((function(cell) {
        var view;
        cell.attr('.uml-class-methods-text/text', '');
        cell.attr('.uml-class-attrs-text/text', '');
        cell.attr('.uml-class-methods-rect').height = 20;
        cell.attr('.uml-class-attrs-rect').height = 20;
        cell.attr('.uml-class-name-rect').height = 40;
        cell.attr('customAttr/isCompact', false);
        view = paper.findViewByModel(cell);
        if (view == null) {
          view = palette.findViewByModel(cell);
        }
        return view.update();
      })(cell));
    }
    return results;
  },
  clear_all: function() {
    var result;
    result = window.confirm('Are you sure you want to clear all the diagram?');
    if (result) {
      return graphMain.clear();
    }
  },
  assign_events: function() {
    $('.btn-hide-methods').on('click', function() {
      return options.general.hide_all_methods();
    });
    $('.btn-show-methods').on('click', function() {
      return options.general.show_all_methods();
    });
    return $('.btn-clear-all').on('click', function() {
      return options.general.clear_all();
    });
  }
};

export var initialize = function() {
  options.general.add_html();
  return options.general.assign_events();
};
