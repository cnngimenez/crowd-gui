// Generated by CoffeeScript 2.5.1
  // toolbar.coffee --
  // Copyright (C) 2019 GILIA

// This program is free software: you can redistribute it and/or modify
  // it under the terms of the GNU Affero General Public License as published by
  // the Free Software Foundation, either version 3 of the License, or
  // (at your option) any later version.

// This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
  // along with this program.  If not, see <http://www.gnu.org/licenses/>.
import {
  generate_json
} from './export.js';

import {
  import_json
} from './import.js';

// @mixin
export var toolbar = {
  // Request the export JSON and show it in the modal.

  // @todo move this to its own module
  show_export: function() {
    var json;
    json = generate_json();
    $("#export-data").val(JSON.stringify(json));
    return $("#modal-export").modal('show');
  },
  // What to do when the user does click on the copy button.

  // @todo move this to its own module
  export_copy_handler: function() {
    $("#export-data").select();
    return document.execCommand("copy");
  },
  // Show the import JSON modal.

  // Fill the textarea with the current diagram if it is empty.

  // @todo move this to its own module
  show_import: function() {
    var exportjson, textarea;
    textarea = $("#import-data");
    if (textarea.val() === '') {
      exportjson = JSON.stringify(generateJSON());
      textarea.val(exportjson);
    }
    return $("#modal-import").modal('show');
  },
  // What to do when the user does click on the improt button.

  // @todo move this to its own module
  do_import_handler: function() {
    var textarea;
    textarea = $("#import-data");
    // load_json_str textarea.val()
    import_json(JSON.parse(textarea.val()));
    return $("#modal-import").modal('hide');
  },
  
  // Assign toolbar events.
  assign_event: function() {
    $("nav button#export-json").on('click', function() {
      return toolbar.show_export();
    });
    $("nav button#import-json").on('click', function() {
      return toolbar.show_import();
    });
    $("nav button#toolbar-clean").on('click', function() {
      return modelcheck.clear_all();
    });
    // Export buttons

    // @todo move this to its own module
    $("div#modal-export button.btn-copy").on('click', function() {
      return toolbar.export_copy_handler();
    });
    // Import buttons

    // @todo move this to its own module
    return $("div#modal-import button.btn-import").on('click', function() {
      return toolbar.do_import_handler();
    });
  }
};

$.when($.ready).then(function() {
  toolbar.assign_event();
  return console.log('toolbar initialized');
});
