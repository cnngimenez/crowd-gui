import {class_options_gui} from './class-options-gui.js'
import {namespace_gui} from './namespace-gui.js'
import {PaletteWidget} from './widgets/palette-widget.js'
import {DiagramWidget} from './widgets/diagram-widget.js'


# Manage the main window, the navbar and main components.
#
# Creates subcomponents.
class MainGui

    constructor: () ->
        @diagram_widget = new DiagramWidget '#paper'
        @namespace_gui = namespace_gui
        @palette_widget = new PaletteWidget "#palette", \
            @diagram_widget.paper, (e) =>
                this._flypaper_mouse_up e
        @class_options_gui = class_options_gui
    
    assign_events: () ->
        # TODO: Move this events to NavbarGui
        $("#btn_namespaces").on 'click', () ->
            namespace_gui.show()
        $("body").on 'mouseup.fly', (evt)  =>
            this._flypaper_mouse_up evt

    # What to do when the use drops while the flypaper is in use.
    #
    # This is here, because the knowledge needed to create the shape when
    # dropping. It needs the main_paper, the flypaper, the type of cell, etc.
    # In other words, this method is responsible to create the cell and to call
    # the mouse_up on the flypaper (i.e. to pass responsability to hide the
    # flypaper).
    # 
    # @param e {Event}
    _flypaper_mouse_up: (evt) ->
        x = evt.pageX
        y = evt.pageY
        if not @palette_widget.is_in_widget evt
            new_element = @palette_widget.get_new_element()
            @diagram_widget.on_flypaper_drop evt, new_element

        @palette_widget.on_flypaper_drop evt

export main_gui = new MainGui
window.main_gui = main_gui  # For debugging purposes

$.when($.ready).then () ->
    main_gui.assign_events()
